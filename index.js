

'use strict'

const XHR = require('core/utils/xhr_async.js');

const ContentNavigator = require('core/content_navigator/index.js');
const Page = require('./page.js');

//const Post = require('./post.js');

require('./style.css');

module.exports = class {
  constructor(qselect, category) {

  }

  static async construct(qselect, category) {
    try {  
      let this_class = new module.exports(qselect, category);

      let div = document.body.querySelector(qselect);

      div.classList.add('posts');

      var post_list = div.querySelector(".post_list");

      let max_index = await XHR.get('/content-manager/posts', {
        command: "max_index",
        category: category 
      });


      let navigator = await ContentNavigator.init({
        page_size: 10,
        max_index: max_index,
        div: div
      }, async function(index, page_size, max_index, content_navigator) {

        if (index === 0) page_size -= 1;
        let npage = await Page.init(
          index, page_size, max_index, content_navigator, category
        ); // TODO: remove max_pages argument as it is never used nor will be...
        hide_loading_overlay();
        return npage;
      });
      await navigator.select_page(0);

      return this_class;
    } catch (e) {
      console.error(e);
      return undefined;
    }
  }
}

